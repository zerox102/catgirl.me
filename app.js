require('dotenv').config()
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var flash = require('connect-flash');
var passport = require('passport'), LocalStrategy = require('passport-local').Strategy;
var session = require('express-session');
//has to be self-defined
var {get_by_uname, authorize_user} = require('./js_modules/user_db');

//Routes
var indexRouter = require('./routes/index');
var userRouter = require('./routes/user');
var hostingRouter = require('./routes/hosting');
var imageRouter = require('./routes/images');

//get app
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//bunch of default stuff
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//storing sessions
app.use(session({ cookie: { maxAge: 60000 }, 
  secret: process.env.SECRET,
  resave: false, 
  saveUninitialized: false}));

//bodyparser for forms
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//flash messages
app.use(flash());

//passport config
passport.use(new LocalStrategy(
  async function(username, password, done) 
  {
    var res = await authorize_user(username, password);

    if(res.err)
        return done(null, false, {message: res.err}); 

    return done(null, res.user);
  }
));

passport.serializeUser(async function(user, done) 
{
  done(null, user.username);
});

passport.deserializeUser(async function(username, done) 
{
  done(null, await get_by_uname(username));
});

app.use(passport.initialize());
app.use(passport.session());

//Routes
app.use('/', indexRouter);
app.use('/user', userRouter);
app.use('/hosting', hostingRouter);
app.use('/images', imageRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) 
{
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
