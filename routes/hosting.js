var {v4} = require('uuid'); 
var express = require('express');
var multer  = require('multer')
var upload = multer({ dest: 'uploads/'});
var storage = multer.memoryStorage();
var upload = multer({ storage: storage });
const { Pool } = require('pg');
const { RateLimiterPostgres } = require('rate-limiter-flexible');

//has to be self-defined
var {change_hosting_key, verify_hosting_key, get_key_user} = require('../js_modules/user_db');
var {verify_image_key, insert_image} = require('../js_modules/image_db');

var router = express.Router();

const client = new Pool({
	database: process.env.BRUTEDATABASE,
	user: process.env.BRUTEUSER,
	password: process.env.BRUTEPASSWORD
});

//settings for api_key generation rate limiter
const opts_generate_key =
{
  storeClient: client,
  points: 5,
  duration: 300, 

  keyPrefix: 'brute_hostingKey',

  blockDuration: 300, 
};

const ready = (err) => 
{
  if (err) 
  {
    console.log(err);
  }
};

const rateLimiter_keygen = new RateLimiterPostgres(opts_generate_key, ready);

/* GET hosting page. */
router.get('/', function(req, res, next) 
{
    if(!req.user)
        return res.redirect('/');

    res.render('hosting', {page:'Hosting - Main', loggedIn : req.user});
});

/* POST upload page, file upload */ 
router.post('/upload', upload.single('file'), async function(req, res, next) 
{
    var user = await get_key_user(req.body.api_key);

    if(!user)
      return res.sendStatus(400);

    user = user.toLowerCase();

    if(!req.file)
      return res.sendStatus(400);

    if(!req.file.originalname.endsWith('.png'))
      return res.sendStatus(400);

    // > 50 mb
    if(req.file.size > 52428800)
      return res.sendStatus(400);

    // generate filename
    var key, max = 0;
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    do
    {
      key = ""
   
      for (var i = 0; i < 4; i++)
      {
        key += possible.charAt(Math.floor(Math.random() * possible.length));
      }

      key += ".png";
       
      max++;
    }
    while(!(await verify_image_key(key, user)) && max < 100);

    if(max == 100)
      return res.sendStatus(500);

    //save image
    if((await insert_image(user, key, req.file.buffer.toString('base64'))))
    {
      var result = "https://" + user + ".catgirl.me/images/" + key;
      return res.send(result);
    }
    else 
    {
      return res.sendStatus(500);
    }
});

/* GET createKey page. */
router.get('/createKey', async function(req, res, next)
{
    if(!req.user || !req.user.hostingVerified)
    {
        res.sendStatus(403);
        return res;
    }

    rateLimiter_keygen.consume(req.user.username).then(async function(rateLimiterPostgres) 
    {
        //create key, then store key, and return key
        var key, max = 0;
        do
        {
          key = v4();
          max++;
        }
        while(!(await verify_hosting_key(key)) && max < 100);
       
        if(max == 100) return res.sendStatus(500);

        if(await change_hosting_key(req.user.username, key))
            return res.send(key);

        return res.sendStatus(500);
    }).catch((rejRes) => 
    {
      if (rejRes instanceof Error)
      {
        console.log(rejRes);
        return res.sendStatus(500);
      }
      else
      {
        res.sendStatus(403);
      }
    })
});

module.exports = router;
