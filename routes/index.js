var express = require('express');
var router = express.Router();

//Guard against site being matched when subdomains are used
router.get('/*', function(req, res, next)
{
  if (!req.subdomains.length || req.subdomains.slice(-1)[0] === 'www') next();
  else
  {
    //Only allow requests to images/
    if(req.path.startsWith("/images/")) next();
    else res.sendStatus(400);
  }
});

/* GET home page. */
router.get('/', function(req, res, next) 
{
  res.render('index', {page:'Home', loggedIn : req.user});
});

module.exports = router;
