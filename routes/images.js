var express = require('express');
var router = express.Router();

var {get_data} = require('../js_modules/image_db');

/* GET hosting page. */
router.get('/:id', async function(req, res, next) 
{
    if (!req.subdomains.length || req.subdomains.slice(-1)[0] === 'www') return res.sendStatus(400);
    if(!req.params.id.endsWith('.png')) return res.sendStatus(400);

    //check if image exists with that subdomain and code
    var subdomain = req.subdomains.slice(-1)[0];
    var filename = req.params.id;

    var data = await get_data(subdomain, filename);
    if(!data) return res.sendStatus(400);
    var img = Buffer.from(data, 'base64');

    res.writeHead(200, {
        'Content-Type': 'image/png',
        'Content-Length': img.length
      });
      res.end(img); 
  });

module.exports = router;
