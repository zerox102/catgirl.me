require('dotenv').config();
var express = require('express');
var router = express.Router();
var passport = require('passport');
var request = require('request');
const { Pool } = require('pg');
const { RateLimiterPostgres } = require('rate-limiter-flexible');
const requestIp = require('request-ip');
//has to be self-defined
var {create_user} = require('../js_modules/user_db');

const client = new Pool({
	database: process.env.BRUTEDATABASE,
	user: process.env.BRUTEUSER,
	password: process.env.BRUTEPASSWORD
});

//settings for login rate limiter
const opts_login =
{
  storeClient: client,
  points: 5,
  duration: 60, 

  keyPrefix: 'brute_login',

  blockDuration: 300, 
};

//settings for register rate limiter
const opts_register =
{
  storeClient: client,
  points: 10,
  duration: 60, 

  keyPrefix: 'brute_register',

  blockDuration: 300, 
};


const ready = (err) => 
{
  if (err) 
  {
    console.log(err);
  }
};

const rateLimiter_login = new RateLimiterPostgres(opts_login, ready);
const rateLimiter_register = new RateLimiterPostgres(opts_register, ready);

/** POST login form, bruteforce protected */
router.post('/login',
  function(req, res, next)
  {
    var auth = passport.authenticate('local', { successRedirect: '/',
    failureRedirect: '/user/login',
    failureFlash: true });

    rateLimiter_login.consume(req.body.username).then((rateLimiterPostgres) => 
    {
      auth(req,res,next);
    }).catch((rejRes) => 
    {
      if (rejRes instanceof Error)
      {
        console.log(rejRes);
        req.flash('error', 'Internal Server Error. Please try again later.');
        res.redirect('/user/login');
      }
      else
      {
        req.flash('error', 'Too many authentication requests. Please try again in 5 minutes.');
        res.redirect('/user/login');
      }
    })
  }
);

/* GET login page. */
router.get('/login', function(req, res, next) 
{
  if(req.user)
    return res.redirect('/');

  var error = req.flash('error');
  res.render('login', {page:'Login', loginErr:error});
});

/* GET logout page (logout user) */ 
router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

/* POST register page (register user), bruteforce protected */
router.post('/register',
  function(req, res, next)
  {

    const formData = 
    {
      secret : process.env.RECAPTCHASECRET,
      response: req.body["g-recaptcha-response"]
    };

    request.post({
      url: 'https://www.google.com/recaptcha/api/siteverify', formData: formData}
      ,function(error, response, body)
       {
         const json =  JSON.parse(body);
          if(error)
          {
            req.flash('error', "Could not verify captcha. Please try again later.");
            res.redirect('/user/register');
          }
          else if (json.success)
          {
            rateLimiter_register.consume(requestIp.getClientIp(req)).then(async function(rateLimiterPostgres)
            {
              var result = await create_user(req.body.username, req.body.email, req.body.password);
              //successful
              if(!result)
              {
                res.redirect('/');
              }
              else
              {
                req.flash('error', result);
                res.redirect('/user/register');
              }

            }).catch((rejRes) => 
            {
              if (rejRes instanceof Error)
              {
                console.log(rejRes);
                req.flash('error', 'Internal Server Error. Please try again later.');
                res.redirect('/user/register');
              }
              else
              {
                req.flash('error', 'Too many registration requests. Please try again in 5 minutes.');
                res.redirect('/user/register');
              }
            })
          }
          else
          {
            req.flash('error', 'Recaptcha verification was unsuccessful.');
            res.redirect('/user/register');
          }
     });
  }
);

/* GET register page. */
router.get('/register', function(req, res, next) 
{
  if(req.user)
    return res.redirect('/');

  var error = req.flash('error');
  res.render('register', {page:'Register', loginErr:error});
});

module.exports = router;
