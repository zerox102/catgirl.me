//Clientside script for the hosting page
$(function() 
{
    //send an ajax request to get a new key, and set the corresponding div if successful. User is aquired from server side (session)
    $("#confirmNewKey").on("click", function () 
    {
        $('#refreshKey').modal('hide');

        $.ajax({
        type: 'GET',
        url: '/hosting/createKey',
        statusCode: 
        {
            403: function(result)
            {
                $('#errorModal').modal('show');
            }
        },
        success: function(result) 
        {
            if(result)
            {
                $('#keyValue').html(result);
                $('#copyKey').html('Copy API Key');
                $("#copyKey").removeAttr("disabled");
            }
            else 
            {
                $('#errorModal').modal('show');
            }
        }
      });
    })

    //copy value of the keyValue div into clipboard
    $("#copyKey").on("click", function () 
    {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($('#keyValue').html()).select();
        document.execCommand("copy");
        $temp.remove();
    })
});